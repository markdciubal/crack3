#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include <sys/stat.h>

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char password[PASS_LEN]; 
    char hash[HASH_LEN];
};

int file_size(char *filename)
{
    struct stat info;
    if (stat(filename, &info) == -1) 
    {
        return -1;
    }
    else
    {
        return info.st_size;
    }
}

int compare(const void *a, const void *b)
{
    return strcmp(((struct entry *)a)->hash, ((struct entry *)b)->hash);
}

int hash_search(const void *a, const void *b)
{
    return strcmp(a, ((struct entry *)b)->hash);
}

int countlines(char *filename)
{
    FILE *f = fopen(filename, "r");
    char line[100];
    int count = 0;
    while (fgets(line, 100, f) != NULL)
    {
        count++;   
    }
    fclose(f);
    return count;
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    int length = file_size(filename);
    char *password = malloc(length);
    int password_count = 0;
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        printf("Can't open %s for reading\n", filename);
        exit(1);
    }
    fread(password, sizeof(char), length, f);
    fclose(f);
    
    for (int i = 0; i < length; i++)
    {
        if (password[i] == '\n')
        {
            password[i] = '\0';
            password_count++;
        }
    }
    *size = password_count;
    struct entry *entries = malloc(password_count * sizeof(struct entry));
    int j = 0;
    for (int i = 0; i < length-1; i++)
    {
        if (password[i] == '\0')
        {
            char *word = &password[i+1];
            char *hash = md5(word, strlen(word));
            strcpy(entries[j].password, word);
            strcpy(entries[j].hash, hash);
            j++; 
        }
    }
    return entries;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    int length = 0;
    // TODO: Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary(argv[2], &length);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, length, sizeof(struct entry), compare);
    // TODO
    // Open the hash file for reading.
    char str[100];
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL)
    {
        perror("Error opening file");
        return(-1);
    }
    while(fgets(str, 100, fp) != NULL)
    {
        str[strlen(str)-1] = '\0';
        struct entry *found = bsearch(str, dict, length, sizeof(struct entry), hash_search);
        if (found)
        {
            printf("%s -> %s\n", str, found->password);
        }
    }
    fclose(fp);
}